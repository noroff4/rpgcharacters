﻿using RPG_Characters.Exceptions;
using RPG_Characters.Items;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPG_Characters.Heroes
{
    public abstract class Hero
    {
        public abstract string Name { get; set; }
        public abstract int Level { get; set; }
        public abstract int Damage { get; set; }
        public abstract List<PrimaryAttribute> BasePrimaryAttributes { get; set; }
        public abstract List<PrimaryAttribute> TotalPrimaryAttributes { get; set; }
        public abstract List<PrimaryAttribute> AttributeIncrementAmount { get; set; }
        public abstract Dictionary<ItemSlot, Weapon> EquippedWeapon { get; set; }
        public abstract Dictionary<ItemSlot, Armor> EquippedArmor { get; set; }
        public abstract List<WeaponType> KnownWeaponTypes { get; set; }
        public abstract List<ArmorType> KnownArmorTypes { get; set; }


        /// <summary>
        ///<para> Calculates Hero Damage based on current weapon and primary attribute </para>
        /// </summary>
        /// <returns>A Decimal, representing character damage</returns>
        public abstract decimal CalculateDamage();


        /// <summary>
        /// <para> Increases hero level by one </para>
        /// <para> Increases attributes based on class of the hero </para>
        /// </summary>
        public void LevelUp()
        {
            Level += 1;
            BasePrimaryAttributes = IncrementAttributes();
            TotalPrimaryAttributes = CalculateTotalAttributes();
            Console.WriteLine("Levelled up!\nCurrent Level: " + Level);
        }
        /// <summary>
        /// Increases BasePrimaryAttributes by AttributeIncrementAmount
        /// </summary>
        /// <returns>Increased BasePrimaryAttributes</returns>
        public List<PrimaryAttribute> IncrementAttributes()
        {
            var newAttributes = BasePrimaryAttributes.Zip(AttributeIncrementAmount, (a, b) => a + b).ToList();
            return newAttributes;
        }
        /// <summary>
        /// Equips <paramref name="armor"/> for object it is called on
        /// </summary>
        /// <param name="armor"></param>
        /// <inheritdoc/>
        /// <returns>string</returns>
        /// <exception cref="InvalidArmorException"></exception>
        public string EquipArmor(Armor armor)
        {
            if (armor.Slot == ItemSlot.Weapon)
            {
                throw new InvalidArmorException("Armor can only be equipped in Armor slot!");
            }
            if (Level < armor.LevelRequirement)
            {
                throw new InvalidArmorException("Armor level too high!");
            }
            if (!KnownArmorTypes.Contains(armor.Type))
            {
                throw new InvalidArmorException("Can't equip for this class!");
            }

            // Replaces current item in given slot
            if (EquippedArmor.ContainsKey(armor.Slot))
            {
                EquippedArmor[armor.Slot] = armor;
            } else 
            {
                EquippedArmor.Add(armor.Slot, armor);
            }

            
            TotalPrimaryAttributes = CalculateTotalAttributes();
            return "New armor equipped!";
        }
        /// <summary>
        /// Equips <paramref name="weapon"/> for character
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>string</returns>
        /// <exception cref="InvalidWeaponException"></exception>
        public string EquipWeapon(Weapon weapon)
        {
            if (weapon.Slot != ItemSlot.Weapon)
            {
                throw new InvalidWeaponException("Weapon can only be equipped in Weapon slot!");
            }

            if (Level < weapon.LevelRequirement)
            {
                throw new InvalidWeaponException("Weapon level too high!");
            }

            if (!KnownWeaponTypes.Contains(weapon.Type))
            {
                throw new InvalidWeaponException("Can't equip for this class!");
            }

            // Replaces current item in given slot
            if (EquippedWeapon.ContainsKey(weapon.Slot))
            {
                EquippedWeapon[weapon.Slot] = weapon;
            } else
            {
                EquippedWeapon.Add(weapon.Slot, weapon);
            }
            return "New weapon equipped!";
        }
        /// <summary>
        ///   <para> Calculates total attributes based on BasePrimaryAttributes and EquippedArmor</para>
        ///   <para> <see cref="BasePrimaryAttributes"/></para>
        ///   <para> <see cref="TotalPrimaryAttributes"/></para>
        ///   <para><see cref="EquippedArmor"/></para>
        /// </summary>
        /// 
        /// <returns>New list of primary attributes</returns>
        private List<PrimaryAttribute> CalculateTotalAttributes()
        {
            var result = new List<PrimaryAttribute>();

            var totalStrength = new PrimaryAttribute();
            var totalDexterity = new PrimaryAttribute();
            var totalIntelligence = new PrimaryAttribute();

            foreach (KeyValuePair<ItemSlot, Armor> armor in EquippedArmor)
            {
                if (armor.Value.PrimaryAttribute.Name == Attribute.Strength)
                {
                    totalStrength += armor.Value.PrimaryAttribute;
                }
                if (armor.Value.PrimaryAttribute.Name == Attribute.Dexterity)
                {
                    totalDexterity += armor.Value.PrimaryAttribute;
                }
                if (armor.Value.PrimaryAttribute.Name == Attribute.Intelligence)
                {
                    totalIntelligence += armor.Value.PrimaryAttribute;
                }
            }
            result.Add(totalStrength);
            result.Add(totalDexterity);
            result.Add(totalIntelligence);

            return BasePrimaryAttributes.Zip(result, (a, b) => a + b).ToList();
        }
        /// <summary>
        /// Prints out hero information
        /// </summary>
        public void GetInformation()
        {
            Console.WriteLine(Name + ", lvl: " + Level);
            Console.WriteLine("====================");
            Console.WriteLine("Base stats:");
            foreach (PrimaryAttribute primaryAttribute in BasePrimaryAttributes)
            {
                Console.WriteLine(primaryAttribute.Name.ToString() + ' ' + primaryAttribute.Value.ToString());
            }

            Console.WriteLine("====================");
            Console.WriteLine("Total stats:");
            foreach (PrimaryAttribute primaryAttribute in TotalPrimaryAttributes)
            {
                Console.WriteLine(primaryAttribute.Name.ToString() + ' ' + primaryAttribute.Value.ToString());
            }

            string equippedArmor = "";
            Console.WriteLine("====================");
            Console.WriteLine();
            foreach (KeyValuePair<ItemSlot, Armor> kvp in EquippedArmor)
            {
                equippedArmor += string.Format("Armor: {1}, Slot: {0}\n", kvp.Key, kvp.Value.Name);
            }
            Console.WriteLine(equippedArmor);

            string equippedWeapon = "";
            foreach (KeyValuePair<ItemSlot, Weapon> kvp in EquippedWeapon)
            {
                equippedWeapon += string.Format("Weapon: {1}\n", kvp.Key, kvp.Value.Name);
            }
            Console.WriteLine(equippedWeapon);
            Console.WriteLine("====================");
            Console.WriteLine("Damage: " + CalculateDamage());
            Console.WriteLine("====================");
        }
    }
}
