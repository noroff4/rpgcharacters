﻿using RPG_Characters.Items;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPG_Characters.Heroes
{
    public class Warrior : Hero
    {
        public override string Name { get; set; }
        public override int Level { get; set; }
        public override int Damage { get; set; }
        public override List<PrimaryAttribute> BasePrimaryAttributes { get; set; }
        public override List<PrimaryAttribute> TotalPrimaryAttributes { get; set; }
        public override List<PrimaryAttribute> AttributeIncrementAmount { get; set; }
        public override Dictionary<ItemSlot, Weapon> EquippedWeapon { get; set; }
        public override Dictionary<ItemSlot, Armor> EquippedArmor { get; set; }
        public override List<WeaponType> KnownWeaponTypes{ get; set; }
        public override List<ArmorType> KnownArmorTypes { get; set; }

        public Warrior(string name)
        {
            Name = name;
            Level = 1;

            // Set level 1 starting attributes
            BasePrimaryAttributes = new List<PrimaryAttribute>();
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Strength, 5));
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Dexterity, 2));
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Intelligence, 1));

            TotalPrimaryAttributes = new List<PrimaryAttribute>();
            TotalPrimaryAttributes.AddRange(BasePrimaryAttributes);

            // Specify amount of gain when levelled up
            AttributeIncrementAmount = new List<PrimaryAttribute>();
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Strength, 3));
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Dexterity, 2));
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Intelligence, 1));

            // Specify weapon types for Warrior class
            KnownWeaponTypes = new List<WeaponType>();
            KnownWeaponTypes.Add(WeaponType.Axe);
            KnownWeaponTypes.Add(WeaponType.Hammer);
            KnownWeaponTypes.Add(WeaponType.Sword);

            // Specify armor types for Warrior class
            KnownArmorTypes = new List<ArmorType>();
            KnownArmorTypes.Add(ArmorType.Mail);
            KnownArmorTypes.Add(ArmorType.Plate);

            EquippedWeapon = new Dictionary<ItemSlot, Weapon>();
            EquippedArmor = new Dictionary<ItemSlot, Armor>();
        }

        public override decimal CalculateDamage()
        {
            if (EquippedWeapon.Count == 0) return 1;
            int currentStrength = 0;
            IEnumerable<PrimaryAttribute> attributeQuery =
                from attribute in TotalPrimaryAttributes
                where attribute.Name == Attribute.Strength
                select attribute;
            foreach (PrimaryAttribute attribute in attributeQuery)
            {
                currentStrength = attribute.Value;
            }
            return EquippedWeapon.Values.Sum(x => x.DamagePerSecond) * (1 + Convert.ToDecimal(currentStrength) / 100);
        }
    }
}
