﻿using RPG_Characters.Items;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPG_Characters.Heroes
{
    public class Rogue : Hero
    {
        public override string Name { get; set; }
        public override int Level { get; set; }
        public override int Damage { get; set; }
        public override List<PrimaryAttribute> BasePrimaryAttributes { get; set; }
        public override List<PrimaryAttribute> TotalPrimaryAttributes { get; set; }
        public override List<PrimaryAttribute> AttributeIncrementAmount { get; set; }
        public override Dictionary<ItemSlot, Weapon> EquippedWeapon { get; set; }
        public override Dictionary<ItemSlot, Armor> EquippedArmor { get; set; }
        public override List<WeaponType> KnownWeaponTypes { get; set; }
        public override List<ArmorType> KnownArmorTypes { get; set; }

        public Rogue(string name)
        {
            Name = name;
            Level = 1;

            BasePrimaryAttributes = new List<PrimaryAttribute>();
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Strength, 2));
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Dexterity, 6));
            BasePrimaryAttributes.Add(new PrimaryAttribute(Attribute.Intelligence, 1));

            TotalPrimaryAttributes = new List<PrimaryAttribute>();
            TotalPrimaryAttributes.AddRange(BasePrimaryAttributes);

            AttributeIncrementAmount = new List<PrimaryAttribute>();
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Strength, 1));
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Dexterity, 4));
            AttributeIncrementAmount.Add(new PrimaryAttribute(Attribute.Intelligence, 1));

            // Specify weapon types for Warrior class
            KnownWeaponTypes = new List<WeaponType>();
            KnownWeaponTypes.Add(WeaponType.Dagger);
            KnownWeaponTypes.Add(WeaponType.Sword);

            // Specify armor types for Warrior class
            KnownArmorTypes = new List<ArmorType>();
            KnownArmorTypes.Add(ArmorType.Mail);
            KnownArmorTypes.Add(ArmorType.Leather);

            EquippedWeapon = new Dictionary<ItemSlot, Weapon>();
            EquippedArmor = new Dictionary<ItemSlot, Armor>();
        }

        public override decimal CalculateDamage()
        {
            if (EquippedWeapon.Count == 0) return 1;
            int currentDexterity = 0;
            IEnumerable<PrimaryAttribute> attributeQuery =
                from attribute in TotalPrimaryAttributes
                where attribute.Name == Attribute.Dexterity
                select attribute;
            foreach (PrimaryAttribute attribute in attributeQuery)
            {
                currentDexterity = attribute.Value;
            }
            return EquippedWeapon.Values.Sum(x => x.DamagePerSecond) * (1 + Convert.ToDecimal(currentDexterity) / 100);
        }
    }
}
