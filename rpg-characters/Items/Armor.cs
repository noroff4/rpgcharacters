﻿using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public class Armor : Item
    {
        public override string Name { get; set; }
        public override int LevelRequirement { get; set; }
        public ArmorType Type { get; set; }
        public override ItemSlot Slot { get; set; }
        public PrimaryAttribute PrimaryAttribute { get; set; }

        public Armor() { }
        public Armor(string name, int levelRequirement, ArmorType type, ItemSlot slot, PrimaryAttribute primaryAttribute)
        {
            Name = name;
            LevelRequirement = levelRequirement;
            Type = type;
            Slot = slot;
            PrimaryAttribute = primaryAttribute;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Level Requirement: {LevelRequirement}";
        }
    }
}
