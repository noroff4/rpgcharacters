﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
    public abstract class Item
    {
        public abstract string Name { get; set; }
        public abstract int LevelRequirement { get; set; }
        public abstract ItemSlot Slot { get; set; }

    }
}
