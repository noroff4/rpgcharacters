﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public enum WeaponType
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
    public class Weapon : Item
    {
        public override string Name { get; set; }
        public WeaponType Type { get; set; }
        public override ItemSlot Slot { get; set; }
        public override int LevelRequirement { get; set; }
        public int Damage { get; set; }
        public decimal AttackSpeed { get; set; }
        public decimal DamagePerSecond
        {
            get
            {
                return (Damage * AttackSpeed);
            }
        }
        public Weapon() { }

        public Weapon(string name, WeaponType type, ItemSlot slot, int levelRequirement, int baseDamage, int attacksPerSecond)
        {
            Name = name;
            Type = type;
            Slot = slot;
            LevelRequirement = levelRequirement;
            Damage = baseDamage;
            AttackSpeed = attacksPerSecond;
        }

        public override string ToString()
        {
            return $"Name: {Name}, Level Requirement: {LevelRequirement}, Damage: {Damage}";
        }

    }
}
