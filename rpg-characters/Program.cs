﻿using RPG_Characters.Exceptions;
using RPG_Characters.Heroes;
using RPG_Characters.Items;
using RPG_Characters.Stats;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static RPG_Characters.Program;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPG_Characters
{
    public class Program
    {
        public enum HeroClass
        {
            Warrior,
            Mage,
            Ranger,
            Rogue
        }

        public enum WeaponRarity
        {
            Common,
            Uncommon,
            Rare,
            Legendary
        }


        static void Main()
        {

            // Get character name + class from user -> create hero
            string userName = GetName();
            HeroClass userClass = GetClass();
            var userCharacter = (Hero)CreateClass(userClass, userName);
            Console.Clear();
            bool running = true;
            while(running)
            {
                Console.WriteLine("Select an option");
                Console.WriteLine("1) Display Stats");
                Console.WriteLine("2) Level Up");
                Console.WriteLine("3) Equip Weapons");
                Console.WriteLine("4) Equip Armor");
                Console.WriteLine("5) Exit");
                
                string chosenOption = Console.ReadLine();
                Console.Clear();
                switch (chosenOption)
                {
                    case "1":
                        userCharacter.GetInformation();
                        break;

                    case "2":
                        userCharacter.LevelUp();
                        break;

                    case "3":
                        try
                        {
                            Console.WriteLine(userCharacter.EquipWeapon(GetWeapon(userCharacter)));
                        } catch(InvalidWeaponException Exception)
                        {
                            Console.Clear();
                            Console.WriteLine(Exception.Message);
                        }
                        break;

                    case "4":
                        try
                        {
                            Console.WriteLine(userCharacter.EquipArmor(GetArmor(userCharacter)));
                        } catch(InvalidArmorException Exception)
                        {
                            Console.Clear();
                            Console.WriteLine(Exception.Message);
                        }
                        break;

                    case "5":
                        running = false;
                        break;

                    default:
                        Console.WriteLine("Not a valid option");
                        break;
                }
            }
            
        }

        /// <summary>
        /// Gets selected armor from randomly generated armor
        /// </summary>
        /// <param name="hero"></param>
        /// <returns>Armor</returns>
        private static Armor GetArmor(Hero hero)
        {
            List<Armor> armorList = GenerateArmor(hero);
            while (true)
            {
                Console.WriteLine("Current Level: " + hero.Level);
                Console.WriteLine("Available armor: ");
                for (int i = 0; i < armorList.Count; i++)
                {
                    Console.WriteLine(i + 1 + ") " + armorList[i]);
                }

                var userOption = Console.ReadLine();
                Console.Clear();

                // Generated armorlist length is hard coded so fixed amount of cases:
                switch (userOption)
                {
                    case "1":
                        return armorList[0];

                    case "2":
                        return armorList[1];

                    case "3":
                        return armorList[2];

                    case "4":
                        return armorList[3];

                    default:
                        Console.Clear();
                        Console.WriteLine("Not a valid option. Choose again");
                        break;
                }
            }

        }
        /// <summary>
        /// Randomly generates different armor pieces
        /// </summary>
        /// <param name="hero"></param>
        /// <returns>List of different armor</returns>
        private static List<Armor> GenerateArmor(Hero hero)
        {
            var armor = new List<Armor>();

            for (int i = 1; i <= 4; i++)
            {
                var armorToAdd = new Armor();
                var guaranteedArmor = new Armor();
                var rand = new Random();

                if (i == 1) // Makes sure to get atleast 1 common type armor, that can always be equipped
                {
                    guaranteedArmor.Type = hero.KnownArmorTypes[rand.Next(hero.KnownArmorTypes.Count)];
                    var rarity = WeaponRarity.Common;
                    var values = Enum.GetValues(typeof(ItemSlot));
                    guaranteedArmor.Slot = (ItemSlot)values.GetValue(rand.Next(values.Length - 1));

                    guaranteedArmor.Name = (rarity + " " + guaranteedArmor.Type + guaranteedArmor.Slot).ToString();
                    
                    guaranteedArmor.LevelRequirement = (int)rarity + 1;
                    if(hero.KnownArmorTypes.Contains(ArmorType.Cloth))
                    {
                        guaranteedArmor.PrimaryAttribute = new PrimaryAttribute() { Name=Attribute.Intelligence, Value=1 };
                    }
                    if (hero.KnownArmorTypes.Contains(ArmorType.Plate))
                    {
                        guaranteedArmor.PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Strength, Value = 1 };
                    }
                    if (hero.KnownArmorTypes.Contains(ArmorType.Leather) || hero.KnownArmorTypes.Contains(ArmorType.Mail))
                    {
                        guaranteedArmor.PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Dexterity, Value = 1 };
                    }

                    armor.Add(guaranteedArmor);
                }
                else
                {
                    armorToAdd.Type = hero.KnownArmorTypes[rand.Next(hero.KnownArmorTypes.Count)];
                    var rarity = Enum.GetName(typeof(WeaponRarity), rand.Next(Enum.GetNames(typeof(WeaponRarity)).Length));
                    var values = Enum.GetValues(typeof(ItemSlot));
                    armorToAdd.Slot = (ItemSlot)values.GetValue(rand.Next(values.Length - 1));

                    armorToAdd.Name = (rarity + " " + armorToAdd.Type + armorToAdd.Slot).ToString();

                    armorToAdd.LevelRequirement = (int)(WeaponRarity)Enum.Parse(typeof(WeaponRarity), rarity) + 1;
                    if (hero.KnownArmorTypes.Contains(ArmorType.Cloth))
                    {
                        armorToAdd.PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Intelligence, Value = armorToAdd.LevelRequirement + 1 };
                    }
                    if (hero.KnownArmorTypes.Contains(ArmorType.Plate))
                    {
                        armorToAdd.PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Strength, Value = armorToAdd.LevelRequirement + 1 };
                    }
                    if (hero.KnownArmorTypes.Contains(ArmorType.Leather) || hero.KnownArmorTypes.Contains(ArmorType.Mail))
                    {
                        armorToAdd.PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Dexterity, Value = armorToAdd.LevelRequirement + 1 };
                    }

                    armor.Add(armorToAdd);
                }
            }

            return armor;
        }

        /// <summary>
        /// Gets selected weapon from randomly generated weapons
        /// </summary>
        /// <returns>Weapon</returns>
        private static Weapon GetWeapon(Hero hero)
        {
            List<Weapon> weaponList = GenerateWeapons(hero);

            while (true)
            {
                Console.WriteLine("Current Level: " + hero.Level);
                Console.WriteLine("Available weapons: ");
                for (int i = 0; i < weaponList.Count; i++)
                {
                    Console.WriteLine(i + 1 + ") " + weaponList[i]);
                }
                var userOption = Console.ReadLine();
                Console.Clear();
                // Generated weaponlist length is hard coded so fixed amount of cases:
                switch (userOption)
                {
                    case "1":
                        return weaponList[0];

                    case "2":
                        return weaponList[1];

                    case "3":
                        return weaponList[2];

                    case "4":
                        return weaponList[3];

                    default:
                        Console.Clear();
                        Console.WriteLine("Not a valid option. Choose again");
                        break;
                }
            }
        }
        /// <summary>
        /// Generate random weapons
        /// </summary>
        /// <param name="hero"></param>
        /// <returns>List of Weapons</returns>
        private static List<Weapon> GenerateWeapons(Hero hero)
        {
            var weapons = new List<Weapon>();

            for (int i = 1; i <= 4; i++)
            {
                var weaponToAdd = new Weapon();
                var guaranteedWeapon = new Weapon();
                var rand = new Random();

                if (i == 1) // Makes sure to get atleast 1 common type weapon, that can always be equipped
                {
                    guaranteedWeapon.Type = hero.KnownWeaponTypes[rand.Next(hero.KnownWeaponTypes.Count)];
                    var rarity = WeaponRarity.Common;
                    guaranteedWeapon.Name = (rarity + " " + guaranteedWeapon.Type).ToString();
                    guaranteedWeapon.Slot = ItemSlot.Weapon;
                    guaranteedWeapon.LevelRequirement = (int)rarity + 1;
                    guaranteedWeapon.Damage = guaranteedWeapon.LevelRequirement * 2;
                    guaranteedWeapon.AttackSpeed = (decimal)((int)guaranteedWeapon.Type + 1) * 2;
                    weapons.Add(guaranteedWeapon);
                }
                else
                {
                    weaponToAdd.Type = hero.KnownWeaponTypes[rand.Next(hero.KnownWeaponTypes.Count)];
                    var weaponRarity = Enum.GetName(typeof(WeaponRarity), rand.Next(Enum.GetNames(typeof(WeaponRarity)).Length));
                    weaponToAdd.Name = (weaponRarity + " " + weaponToAdd.Type).ToString();
                    weaponToAdd.Slot = ItemSlot.Weapon;
                    weaponToAdd.LevelRequirement = (int)(WeaponRarity)Enum.Parse(typeof(WeaponRarity), weaponRarity) + 1;
                    weaponToAdd.Damage = weaponToAdd.LevelRequirement * 2;
                    weaponToAdd.AttackSpeed = (decimal)((int)weaponToAdd.Type + 1) * 2;
                    weapons.Add(weaponToAdd);
                }
            }

            return weapons;
        }


        /// <summary>
        /// Get class type from console
        /// </summary>
        /// <returns>HeroClass enum representing class type</returns>
        private static HeroClass GetClass()
        {
            while(true)
            {
                Console.WriteLine("Choose a class");
                Console.WriteLine("1) Warrior");
                Console.WriteLine("2) Ranger");
                Console.WriteLine("3) Rogue");
                Console.WriteLine("4) Mage");
                string chosenClassIndex = Console.ReadLine();

                switch (chosenClassIndex)
                {
                    case "1":
                        return HeroClass.Warrior;
                    case "2":
                        return HeroClass.Ranger;
                    case "3":
                        return HeroClass.Rogue;
                    case "4":
                        return HeroClass.Mage;
                    default:
                        Console.WriteLine("Please input 1, 2, 3 or 4");
                        break;
                }
            }
        }

        /// <summary>
        /// Get username from console
        /// </summary>
        /// <returns>string representing username</returns>
        private static string GetName()
        {
            while(true)
            {
                Console.Clear();
                Console.WriteLine("Name your character:");
                var name = Console.ReadLine();
                if (name != null)
                {
                    return name;
                }
            }
        }
        /// <summary>
        /// Dynamically create object from different class
        /// </summary>
        /// <param name="heroClass"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static object CreateClass(HeroClass heroClass, string name)
        {
            var ns = typeof(HeroClass).Namespace;
            var typeName = ns + ".Heroes." + heroClass.ToString();

            return Activator.CreateInstance(Type.GetType(typeName), new string[] { name });
        }
    }
}