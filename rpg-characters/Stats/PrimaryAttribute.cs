﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Stats
{
    public enum Attribute
    {
        Strength,
        Dexterity,
        Intelligence
    }

    public class PrimaryAttribute
    {

        public Attribute Name { get; set; }
        public int Value { get; set; }

        public PrimaryAttribute() { }
        public PrimaryAttribute(Attribute name, int value)
        {
            Name = name;
            Value = value;
        }

        public static PrimaryAttribute operator +(PrimaryAttribute lhs, PrimaryAttribute rhs)
        {
            return new PrimaryAttribute(lhs.Name, lhs.Value + rhs.Value);
        }
    }
}
