# RPG CHARACTERS

## Includes folders for unit tests and source code
 - Navigate to */rpg-characters* for **source code**
 - Navigate to */RPGCharactersTests* for **unit tests**
### Features
 - Name your character
 - Choose a class
    - Warrior
    - Mage
    - Ranger
    - Rogue
 - Level up character
    - Increases attributes based on class
 - Randomly generated weapons and armor for each class
 - Equip weapons and armor based on class and level
    - Weapons affect characters total damage
    - Armor increases character attributes
 - Display stats and items
    - Strength
    - Intelligence
    - Dexterity
    - Damage
    - Equipped items

### Unit tests
 - Unit tests can be found in RPGCharacterTests-folder
 - All unit tests provided in instructions are passing

