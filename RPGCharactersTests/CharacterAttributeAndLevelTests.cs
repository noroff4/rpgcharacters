﻿
using RPG_Characters.Heroes;
using RPG_Characters.Items;
using RPG_Characters.Stats;
using FluentAssertions;

using Attribute = RPG_Characters.Stats.Attribute;

namespace RPGCharactersTests
{
    public class CharacterAttributeAndLevelTests
    {

        [Fact]
        public void WarriorLevel_OnCreation_ShouldBeLevel1()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            int expected = 1;
            //Act
            int actual = testWarrior.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpFrom1_ShouldBeLevel2()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            testWarrior.LevelUp();
            int expected = 2;
            //Act
            int actual = testWarrior.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_OnCreationAttributes_ShouldBe1_1_8()
        {
            //Arrange
            Mage testMage = new Mage("Test Mage");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 1));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 1));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 8));
            //Act
            List<PrimaryAttribute> actual = testMage.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Warrior_OnCreationAttributes_ShouldBe5_2_1()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 5));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 2));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 1));
            //Act
            List<PrimaryAttribute> actual = testWarrior.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Ranger_OnCreationAttributes_ShouldBe1_7_1()
        {
            //Arrange
            Ranger testRanger = new Ranger("Test Ranger");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 1));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 7));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 1));
            //Act
            List<PrimaryAttribute> actual = testRanger.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void Rogue_OnCreationAttributes_ShouldBe2_6_1()
        {
            //Arrange
            Rogue testRogue = new Rogue("Test Rogue");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 2));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 6));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 1));
            //Act
            List<PrimaryAttribute> actual = testRogue.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }


        [Fact]
        public void MageLevelUp_OnLevelUpAttributes_ShouldBe2_2_13()
        {
            //Arrange
            Mage testMage = new Mage("Test Mage");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 2));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 2));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 13));
            testMage.LevelUp();
            //Act
            List<PrimaryAttribute> actual = testMage.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WarriorLevelUp_OnLevelUpAttributes_ShouldBe8_4_2()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 8));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 4));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 2));
            testWarrior.LevelUp();
            //Act
            List<PrimaryAttribute> actual = testWarrior.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void RangerLevelUp_OnLevelUpAttributes_ShouldBe2_12_2()
        {
            //Arrange
            Ranger testRanger = new Ranger("Test Ranger");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 2));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 12));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 2));
            testRanger.LevelUp();
            //Act
            List<PrimaryAttribute> actual = testRanger.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void RogueLevelUp_OnLevelUpAttributes_ShouldBe3_10_2()
        {
            //Arrange
            Rogue testRogue = new Rogue("Test Rogue");
            List<PrimaryAttribute> expected = new List<PrimaryAttribute>();
            expected.Add(new PrimaryAttribute(Attribute.Strength, 3));
            expected.Add(new PrimaryAttribute(Attribute.Dexterity, 10));
            expected.Add(new PrimaryAttribute(Attribute.Intelligence, 2));
            testRogue.LevelUp();
            //Act
            List<PrimaryAttribute> actual = testRogue.BasePrimaryAttributes;
            // Assert
            // Using FluentAssertions for easier collection testing
            actual.Should().BeEquivalentTo(expected);
        }
    }
}