﻿using RPG_Characters.Exceptions;
using RPG_Characters.Heroes;
using RPG_Characters.Items;
using RPG_Characters.Stats;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPGCharactersTests
{
    public class HeroTests
    {
        [Fact]
        public void EquipWeapon_TriesToEquipHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testWeapon = new Weapon()
            {
                Name = "Uncommon axe",
                LevelRequirement = 2,
                Slot = ItemSlot.Weapon,
                Type = WeaponType.Axe,
                Damage = 7,
                AttackSpeed = 1.1m
            };
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testWeapon));

        }

        [Fact]
        public void EquipWeapon_TriesToEquipHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testArmor = new Armor()
            {
                Name = "Uncommon Platebody",
                LevelRequirement = 2,
                Slot = ItemSlot.Body,
                Type = ArmorType.Plate,
                PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Strength, Value = 1 }
            };
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testArmor));
        }

        [Fact]
        public void EquipWeapon_TriesToEquipWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testWeapon = new Weapon()
            {
                Name = "Common bow",
                LevelRequirement = 1,
                Slot = ItemSlot.Weapon,
                Type = WeaponType.Bow,
                Damage = 10,
                AttackSpeed = 1.0m
            };
            //Act & Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeapon(testWeapon));

        }

        [Fact]
        public void EquipWeapon_TriesToEquipWrongArmorType_ShouldThrowInvalidArmorException()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testArmor = new Armor()
            {
                Name = "Common cloth hood",
                LevelRequirement = 1,
                Slot = ItemSlot.Head,
                Type = ArmorType.Cloth,
                PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Intelligence, Value = 5 }
            };
            //Act & Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmor(testArmor));
        }

        [Fact]
        public void EquipWeapon_TriesToEquipSuccesfully_ShouldReturnSuccessString()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testWeapon = new Weapon()
            {
                Name = "Uncommon axe",
                LevelRequirement = 1,
                Slot = ItemSlot.Weapon,
                Type = WeaponType.Axe,
                Damage = 7,
                AttackSpeed = 1.1m
            };
            string expected = "New weapon equipped!";
            //Act
            string actual = testWarrior.EquipWeapon(testWeapon);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_TriesToEquipSuccesfully_ShouldReturnSuccessString()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testArmor = new Armor()
            {
                Name = "Common Platebody",
                LevelRequirement = 1,
                Slot = ItemSlot.Body,
                Type = ArmorType.Plate,
                PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Strength, Value = 1 }
            };
            string expected = "New armor equipped!";
            //Act
            string actual = testWarrior.EquipArmor(testArmor);
            // Assert
            Assert.Equal(expected, actual);
        }

    }
}