using RPG_Characters.Heroes;
using RPG_Characters.Items;
using RPG_Characters.Stats;
using Attribute = RPG_Characters.Stats.Attribute;

namespace RPGCharactersTests
{
    public class WarriorTests
    {

        [Fact]
        public void Warrior_OnCreation_ShouldBeLevel1()
        {
            //Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            int expected = 1;
            //Act
            int actual = testWarrior.Level;
            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void CalculateDamage_CalculatesWarriorTotalDamageWithWeapon_ShouldReturnDecimalValue()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                Slot = ItemSlot.Weapon,
                Type = WeaponType.Axe,
                Damage = 7,
                AttackSpeed = 1.1m
            };
            testWarrior.EquipWeapon(testAxe);
   
            decimal expected = (7m * 1.1m) * (1m + 5m / 100m);

            // Act
            decimal actual = testWarrior.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_CalculatesWarriorTotalDamageWithoutWeapon_ShouldReturnDecimalValue()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test Warrior");

            decimal expected = 1;

            // Act
            decimal actual = testWarrior.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_CalculatesWarriorTotalDamageWithWeaponAndArmor_ShouldReturnDecimalValue()
        {
            // Arrange
            Warrior testWarrior = new Warrior("Test Warrior");
            var testAxe = new Weapon()
            {
                Name = "Common axe",
                LevelRequirement = 1,
                Slot = ItemSlot.Weapon,
                Type = WeaponType.Axe,
                Damage = 7,
                AttackSpeed = 1.1m
            };

            var testArmor = new Armor()
            {
                Name = "Common Platebody",
                LevelRequirement = 1,
                Slot = ItemSlot.Body,
                Type = ArmorType.Plate,
                PrimaryAttribute = new PrimaryAttribute() { Name = Attribute.Strength, Value = 1 }
            };
            testWarrior.EquipWeapon(testAxe);
            testWarrior.EquipArmor(testArmor);

            decimal expected = (7m * 1.1m) * (1m + 6m / 100m);

            // Act
            decimal actual = testWarrior.CalculateDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

    }
}